# MPIC Docker Test Environment

This is a dockerized PostgreSQL test environment. It includes only a container with the PostgreSQL database engine and the database already created with some sample data.

## Getting started

Just type `docker compose up -d` and a container called `mpic-database` will start. It exposes the PostgreSQL port (`5432`) just in case you need to connect using a PostgreSQL client from your laptop.

The needed data to connect to the database is included in the `.env` file.

## Sample data

at `db\init.sql` there are some SQL code to create the tables and insert some sample data.