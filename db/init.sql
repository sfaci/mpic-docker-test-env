GRANT ALL PRIVILEGES ON DATABASE mpic to "postgres";

\c mpic;   

CREATE TYPE compliance_requirement AS ENUM ('legal', 'gdpr');

CREATE TYPE contextual_attribute AS ENUM (
    'agent_app_install_id',
    'agent_client_platform',
    'agent_client_platform_family',
    'page_id',
    'page_title',
    'page_namespace',
    'page_namespace_name',
    'page_revision_id',
    'page_wikidata_id',
    'page_wikidata_qid',
    'page_content_language',
    'page_is_redirect',
    'page_user_groups_allowed_to_move',
    'page_user_groups_allowed_to_edit',
    'mediawiki_skin',
    'mediawiki_version',
    'mediawiki_is_production',
    'mediawiki_is_debug_mode',
    'mediawiki_database',
    'mediawiki_site_content_language',
    'mediawiki_site_content_language_variant',
    'performer_is_logged_in',
    'performer_id',
    'performer_name',
    'performer_session_id',
    'performer_pageview_id',
    'performer_groups',
    'performer_is_bot',
    'performer_language',
    'performer_language_variant',
    'performer_can_probably_edit_page',
    'performer_edit_count',
    'performer_edit_count_bucket',
    'performer_registration_dt'
);

CREATE TYPE environment AS ENUM ('development', 'staging', 'production', 'external');

CREATE TABLE IF NOT EXISTS instruments (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    slug varchar(255) NOT NULL,
    description text,
    creator varchar(255) NOT NULL,
    owner varchar(255) NOT NULL,
    purpose varchar(255),
    created_at timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    start_date timestamptz DEFAULT CURRENT_TIMESTAMP NOT NULL,
    end_date timestamptz,
    task text NOT NULL,
    compliance_requirements compliance_requirement ARRAY NOT NULL,
    sample_unit varchar(255) NOT NULL,
    sample_rate REAL NOT NULL,
    contextual_attributes contextual_attribute ARRAY NOT NULL,
    environments environment ARRAY NOT NULL
);

CREATE TABLE IF NOT EXISTS instrument_sample_rates (
    instrument_id integer references instruments (id),
    dblist varchar(255) NOT NULL,
    sample_rate REAL NOT NULL
);

INSERT INTO instruments (id, name, slug, description, creator, owner, purpose, task, compliance_requirements, sample_unit, sample_rate, contextual_attributes, environments) VALUES 
(1, 'Web Scroll UI', 'web-scroll-ui', 'abc description', 'Jane Doe', 'Web Team', 'purpose 1', 'task 1', '{legal}', 'pageview', 0.25, '{page_id,page_title}', '{development}'), 
(2, 'Desktop UI Interactions', 'desktop-ui-interactions', 'efd description', 'Jill Hill', 'Editing Team', 'purpose 1', 'task 1', '{gdpr}', 'session', 0.5, '{performer_id,page_wikidata_qid}', '{staging}'), 
(3, 'Android Article Find In Page', 'android-article-find-in-page', 'xyz description', 'Julie Doe', 'Android Team', 'purpose 1', 'task 1', '{legal}', 'pageview', 1.0, '{page_id,performer_id}','{production}')
